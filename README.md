# Getting started with git

## Introduction
This **git repository** is created as a demo remote repo for the VLSI 2021 . Refer online for more markdown syntax.

## Creating a Local Repository: Commands List

- `git init` 
- `git config --local user.name YOUR_NAME`
- `git config --local user.email EMAIL`
- add / change some files
- `git status -s` or `git status`
- `git add FILENAME(S)`
- `git commit -m "COMMIT MSG DESCRIBING CHANGES"`
- `git log --oneline`

## Creating a Demo Remote Repo - on Bitbucket.org

We can use the GUI to perform the following actions that result in creation of a remote repo

- create repo
- add a file
- commit
- view changes

You have created a local repo 
You have created a Demo remote repo